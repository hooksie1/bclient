package bclient

import "testing"

func TestNewKV(t *testing.T) {
	b := NewBucket("Test")
	k := NewKV().SetBucket(b).SetKey("test").SetValue("test")

	if k.Key != "test" {
		t.Errorf("key is not right")
	}

	if b.Name != k.Bucket.Name {
		t.Errorf("bucket reference doesn't match")
	}

	if k.Key != "test" {
		t.Errorf("uh oh spaghettios")
	}

	if k.Key == "whoa" {
		t.Errorf("something's wrong")
	}

}
